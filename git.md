# git基本操作

此指南仅适用于小白

更高级的操作请参看  [tutorials](https://es.atlassian.com/git/tutorials)

## 获取代码

```shell
git clone https://git.ustc.edu.cn/bdaa-kg/kg-meta.git
```

## 创建新分支(可选)

获取开发分支代码

```shell
git checkout dev
git pull
```

新建一个开发分支

```shell
git checkout -b feature_name
```

## 加入新内容到主分支

新增内容

```shell
git add .
```


```shell
git commit -m "the message to describe the modification"
git push origin branch_name:dev
```

注：如果是在dev分支上直接开发的话，可以直接

```shell
git push
```

提交分支修改请求，如下图


## 注意事项

* 每次开发前记得同步

## Q&A

1. 如果我在本地开发的时候误切到master分支上，而且add，commit了，但是没办法push到master上去要怎么处理（切到dev上，发现修改的内容也没有了）？

   答：可以尝试先新建一个分支

   ```shell
   git checkout -b new_branch
   ```

   然后将新分支推送到remote的dev分支上

   ```shell
   git push origin new_branch:dev
   ```

## 其它常用命令

### 分支相关

* 删除分支（不要删除自己的master和dev）

  ```shell
  git branch -d branch_name
  ```

* 查看分支

  ```shell
  git branch -a
  ```

  

